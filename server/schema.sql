--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: jwt_token; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.jwt_token AS (
	role text,
	exp integer,
	name character varying,
	user_id integer
);


--
-- Name: add_move_stock_context(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.add_move_stock_context() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
          NEW.user_id = current_setting('jwt.claims.user_id', false);
          NEW.created_at = current_timestamp;
          IF NEW.user_id IS NULL THEN
            RAISE EXCEPTION 'Cannot have null s user_id';
          END IF;
          RETURN NEW;
        END;
      $$;


--
-- Name: move_stock(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.move_stock() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        DECLARE
          old_stock stocks;
        BEGIN
          SELECT stocks.* INTO old_stock FROM stocks
            WHERE product_id = NEW.product_id AND location_code = NEW.location_code;

          IF old_stock.id IS NULL THEN
            INSERT INTO stocks (product_id, location_code, quantity)
              VALUES (NEW.product_id, NEW.location_code, NEW.quantity);
          ELSE
            UPDATE stocks SET quantity = old_stock.quantity + NEW.quantity
              WHERE product_id = NEW.product_id AND location_code = NEW.location_code;
          END IF;
          RETURN NULL;
        END;
      $$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.products (
    id integer NOT NULL,
    name character varying(1000) NOT NULL,
    "desc" text,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: products_quantity(public.products); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.products_quantity(prod public.products) RETURNS integer
    LANGUAGE plpgsql STABLE
    AS $$
        BEGIN
          RETURN SUM(quantity) FROM stocks WHERE product_id = prod.id;
        END;
      $$;


--
-- Name: sign_in(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sign_in(name text, password text) RETURNS public.jwt_token
    LANGUAGE plpgsql
    AS $$
    DECLARE
      current_account users;
    BEGIN
      SELECT users.* INTO current_account FROM users
        WHERE users.name = sign_in.name;

      if current_account.password_hash = crypt(sign_in.password, current_account.password_hash) then
        RETURN (
          current_account.role,
          extract(epoch from now() + interval '7 days'),
          current_account.name,
          current_account.id
        )::jwt_token;
      else
          RETURN null;
      end if;
    END;
    $$;


--
-- Name: sign_up(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sign_up(name text, password text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    BEGIN
      INSERT INTO users (name, role, password_hash)
        VALUES (sign_up.name, 'staff', crypt(sign_up.password, gen_salt('bf', 8)));
      RETURN true;
    END;
    $$;


--
-- Name: pgmigrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pgmigrations (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    run_on timestamp without time zone NOT NULL
);


--
-- Name: TABLE pgmigrations; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.pgmigrations IS '@omit';


--
-- Name: pgmigrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pgmigrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pgmigrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pgmigrations_id_seq OWNED BY public.pgmigrations.id;


--
-- Name: product_movements; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.product_movements (
    id integer NOT NULL,
    product_id integer NOT NULL,
    user_id integer NOT NULL,
    location_code character varying(1000) NOT NULL,
    quantity integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE product_movements; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.product_movements IS '@omit update,delete';


--
-- Name: product_movements_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.product_movements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_movements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.product_movements_id_seq OWNED BY public.product_movements.id;


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: stocks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stocks (
    id integer NOT NULL,
    product_id integer NOT NULL,
    location_code character varying(1000) NOT NULL,
    quantity integer NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE stocks; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.stocks IS '@omit create,update,delete';


--
-- Name: stocks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stocks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stocks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.stocks_id_seq OWNED BY public.stocks.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(1000) NOT NULL,
    role character varying(1000) NOT NULL,
    password_hash character varying(1000) NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE users; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.users IS '@omit delete,create';


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: pgmigrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pgmigrations ALTER COLUMN id SET DEFAULT nextval('public.pgmigrations_id_seq'::regclass);


--
-- Name: product_movements id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_movements ALTER COLUMN id SET DEFAULT nextval('public.product_movements_id_seq'::regclass);


--
-- Name: products id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: stocks id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stocks ALTER COLUMN id SET DEFAULT nextval('public.stocks_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: pgmigrations pgmigrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pgmigrations
    ADD CONSTRAINT pgmigrations_pkey PRIMARY KEY (id);


--
-- Name: product_movements product_movements_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_movements
    ADD CONSTRAINT product_movements_pkey PRIMARY KEY (id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: stocks stocks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stocks
    ADD CONSTRAINT stocks_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: products_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX products_name_index ON public.products USING btree (name);


--
-- Name: users_name_unique_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX users_name_unique_index ON public.users USING btree (name);


--
-- Name: product_movements add_move_stock_context; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER add_move_stock_context BEFORE INSERT ON public.product_movements FOR EACH ROW EXECUTE PROCEDURE public.add_move_stock_context();


--
-- Name: product_movements move_stock; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER move_stock AFTER INSERT ON public.product_movements FOR EACH ROW EXECUTE PROCEDURE public.move_stock();


--
-- Name: product_movements product_movements_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_movements
    ADD CONSTRAINT product_movements_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.products(id) ON DELETE CASCADE;


--
-- Name: product_movements product_movements_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_movements
    ADD CONSTRAINT product_movements_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: stocks stocks_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stocks
    ADD CONSTRAINT stocks_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.products(id) ON DELETE CASCADE;


--
-- Name: TABLE products; Type: ACL; Schema: public; Owner: -
--

GRANT INSERT,UPDATE ON TABLE public.products TO staff;
GRANT SELECT ON TABLE public.products TO anonymous;


--
-- Name: SEQUENCE pgmigrations_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.pgmigrations_id_seq TO anonymous;


--
-- Name: TABLE product_movements; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT ON TABLE public.product_movements TO staff;


--
-- Name: SEQUENCE product_movements_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.product_movements_id_seq TO anonymous;


--
-- Name: SEQUENCE products_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.products_id_seq TO anonymous;


--
-- Name: TABLE stocks; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT ON TABLE public.stocks TO anonymous;


--
-- Name: SEQUENCE stocks_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.stocks_id_seq TO anonymous;


--
-- Name: TABLE users; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT ON TABLE public.users TO anonymous;


--
-- Name: SEQUENCE users_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.users_id_seq TO anonymous;


--
-- PostgreSQL database dump complete
--

