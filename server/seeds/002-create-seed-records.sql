BEGIN;
  SET local jwt.claims.user_id TO '2';
  \COPY products FROM './seeds/csv/products.csv' WITH (FORMAT csv);
  \COPY product_movements FROM './seeds/csv/product_movements.csv' WITH (FORMAT csv);
COMMIT;
SELECT product_id, SUM(quantity) FROM stocks GROUP BY product_id;
