DO $$
BEGIN
  IF (SELECT COUNT(*) FROM users where name='admin')=0 THEN
    PERFORM sign_up('admin', 'correcthorsebatterystaple');
    UPDATE users SET role='admin' where name='admin';
  END IF;

  IF (SELECT COUNT(*) FROM users where name='keang')=0 THEN
    PERFORM sign_up('keang', '1234');
  END IF;
END;
$$
