exports.shorthands = undefined

exports.up = (pgm) => {
  pgm.sql(
    `
      CREATE FUNCTION products_quantity(prod public.products) RETURNS integer as $products_quantity$
        BEGIN
          RETURN SUM(quantity) FROM stocks WHERE product_id = prod.id;
        END;
      $products_quantity$ LANGUAGE plpgsql STABLE;
    `
  )
}

exports.down = (pgm) => {
  pgm.sql('DROP FUNCTION products_quantity(prod public.products)')
}
