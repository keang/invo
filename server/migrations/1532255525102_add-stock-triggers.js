exports.shorthands = undefined

exports.up = (pgm) => {
  pgm.sql(
    `
      CREATE FUNCTION add_move_stock_context() RETURNS trigger AS $add_move_stock_context$
        BEGIN
          NEW.user_id = current_setting('jwt.claims.user_id', false);
          NEW.created_at = current_timestamp;
          IF NEW.user_id IS NULL THEN
            RAISE EXCEPTION 'Cannot have null s user_id';
          END IF;
          RETURN NEW;
        END;
      $add_move_stock_context$ LANGUAGE plpgsql;

      CREATE FUNCTION move_stock() RETURNS trigger AS $move_stock$
        DECLARE
          old_stock stocks;
        BEGIN
          SELECT stocks.* INTO old_stock FROM stocks
            WHERE product_id = NEW.product_id AND location_code = NEW.location_code;

          IF old_stock.id IS NULL THEN
            INSERT INTO stocks (product_id, location_code, quantity)
              VALUES (NEW.product_id, NEW.location_code, NEW.quantity);
          ELSE
            UPDATE stocks SET quantity = old_stock.quantity + NEW.quantity
              WHERE product_id = NEW.product_id AND location_code = NEW.location_code;
          END IF;
          RETURN NULL;
        END;
      $move_stock$ LANGUAGE plpgsql;

      CREATE TRIGGER add_move_stock_context BEFORE INSERT on product_movements
        FOR EACH ROW EXECUTE PROCEDURE add_move_stock_context();
      CREATE TRIGGER move_stock AFTER INSERT on product_movements
        FOR EACH ROW EXECUTE PROCEDURE move_stock();
    `
  )
}

exports.down = (pgm) => {
  pgm.sql(
    `
      DROP TRIGGER add_move_stock_context on product_movements;
      DROP FUNCTION add_move_stock_context();
      DROP TRIGGER move_stock on product_movements;
      DROP FUNCTION move_stock();
    `
  )
}
