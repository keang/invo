exports.shorthands = undefined

exports.up = (pgm) => {
  pgm.createTable('products', {
    id: 'id',
    name: { type: 'varchar(1000)', notNull: true },
    desc: { type: 'text' },
    updated_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp')
    },
    created_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp')
    }
  })
  pgm.createIndex('products', 'name')
}

exports.down = (pgm) => {
  pgm.dropTable('products')
}
