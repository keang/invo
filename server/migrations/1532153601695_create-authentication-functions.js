exports.shorthands = undefined

exports.up = (pgm) => {
  pgm.createExtension('pgcrypto')
  pgm.createFunction('sign_in',
    ['name text', 'password text'],
    {
      returns: 'jwt_token',
      language: 'plpgsql'
    },
    `
    DECLARE
      current_account users;
    BEGIN
      SELECT users.* INTO current_account FROM users
        WHERE users.name = sign_in.name;

      if current_account.password_hash = crypt(sign_in.password, current_account.password_hash) then
        RETURN (
          current_account.role,
          extract(epoch from now() + interval '7 days'),
          current_account.name,
          current_account.id
        )::jwt_token;
      else
          RETURN null;
      end if;
    END;
    `
  )
  pgm.createFunction('sign_up',
    ['name text', 'password text'],
    {
      returns: 'boolean',
      language: 'plpgsql'
    },
    `
    BEGIN
      INSERT INTO users (name, role, password_hash)
        VALUES (sign_up.name, 'staff', crypt(sign_up.password, gen_salt('bf', 8)));
      RETURN true;
    END;
    `
  )
}

exports.down = (pgm) => {
  pgm.dropFunction('sign_up',
    ['name text', 'password text']
  )
  pgm.dropFunction('sign_in',
    ['name text', 'password text']
  )
  pgm.dropExtension('pgcrypto')
}
