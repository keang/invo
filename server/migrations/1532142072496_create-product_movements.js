exports.shorthands = undefined

exports.up = (pgm) => {
  pgm.createTable('product_movements', {
    id: 'id',
    product_id: {
      type: 'integer',
      notNull: true,
      references: '"products"',
      onDelete: 'cascade'
    },
    user_id: {
      type: 'integer',
      notNull: true,
      references: '"users"',
      onDelete: 'cascade'
    },
    location_code: { type: 'varchar(1000)', notNull: true },
    quantity: { type: 'integer', notNull: true },
    created_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp')
    }
  })
}

exports.down = (pgm) => {
  pgm.dropTable('product_movements')
}
