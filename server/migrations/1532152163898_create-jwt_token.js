exports.shorthands = undefined

exports.up = (pgm) => {
  pgm.createType('jwt_token', {
    role: 'text',
    exp: 'integer',
    name: 'varchar',
    user_id: 'integer'
  })
}

exports.down = (pgm) => {
  pgm.dropType('jwt_token')
}
