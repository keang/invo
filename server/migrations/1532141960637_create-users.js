exports.shorthands = undefined

exports.up = (pgm) => {
  pgm.createTable('users', {
    id: 'id',
    name: { type: 'varchar(1000)', notNull: true },
    role: { type: 'varchar(1000)', notNull: true },
    password_hash: { type: 'varchar(1000)', notNull: true },
    updated_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp')
    },
    created_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp')
    }
  })
  pgm.createIndex('users', 'name', {unique: true})
}

exports.down = (pgm) => {
  pgm.dropTable('users')
}
