exports.shorthands = undefined

exports.up = (pgm) => {
  pgm.sql(
    `
      DO $$
      BEGIN
        CREATE ROLE anonymous WITH NOLOGIN;
        EXCEPTION WHEN OTHERS THEN
        RAISE NOTICE 'not creating role anonymous -- it already exists';
      END
      $$;

      DO $$
      BEGIN
        CREATE ROLE staff WITH NOLOGIN;
        EXCEPTION WHEN OTHERS THEN
        RAISE NOTICE 'not creating role staff -- it already exists';
      END
      $$;

      GRANT INSERT on TABLE products, product_movements to staff;
      GRANT INSERT on TABLE users TO admin;

      GRANT SELECT on TABLE users, products, stocks TO anonymous;
      GRANT SELECT on TABLE product_movements TO staff;

      GRANT UPDATE on TABLE users to admin;
      GRANT UPDATE on TABLE products to staff;

      GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO anonymous;

      GRANT anonymous TO staff;
      GRANT staff TO admin;
    `
  )
}

exports.down = (pgm) => {
  pgm.sql(
    `
      REASSIGN OWNED BY anonymous TO admin;
      DROP OWNED BY anonymous;
      DROP ROLE anonymous;
      REASSIGN OWNED BY staff TO admin;
      DROP OWNED BY staff;
      DROP ROLE staff;
    `
  )
}
