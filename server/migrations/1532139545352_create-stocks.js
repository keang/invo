exports.shorthands = undefined

exports.up = (pgm) => {
  pgm.createTable('stocks', {
    id: 'id',
    product_id: {
      type: 'integer',
      notNull: true,
      references: '"products"',
      onDelete: 'cascade'
    },
    location_code: { type: 'varchar(1000)', notNull: true },
    quantity: {
      type: 'integer',
      notNull: true
    },
    updated_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp')
    },
    created_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp')
    }
  })
}

exports.down = (pgm) => {
  pgm.dropTable('stocks')
}
