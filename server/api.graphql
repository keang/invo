"""All input for the create `Product` mutation."""
input CreateProductInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """The `Product` to be created by this mutation."""
  product: ProductInput!
}

"""All input for the create `ProductMovement` mutation."""
input CreateProductMovementInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """The `ProductMovement` to be created by this mutation."""
  productMovement: ProductMovementInput!
}

"""The output of our create `ProductMovement` mutation."""
type CreateProductMovementPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `ProductMovement` that was created by this mutation."""
  productMovement: ProductMovement

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """Reads a single `Product` that is related to this `ProductMovement`."""
  productByProductId: Product

  """Reads a single `User` that is related to this `ProductMovement`."""
  userByUserId: User

  """An edge for our `ProductMovement`. May be used by Relay 1."""
  productMovementEdge(
    """The method to use when ordering `ProductMovement`."""
    orderBy: [ProductMovementsOrderBy!] = PRIMARY_KEY_ASC
  ): ProductMovementsEdge
}

"""The output of our create `Product` mutation."""
type CreateProductPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `Product` that was created by this mutation."""
  product: Product

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """An edge for our `Product`. May be used by Relay 1."""
  productEdge(
    """The method to use when ordering `Product`."""
    orderBy: [ProductsOrderBy!] = PRIMARY_KEY_ASC
  ): ProductsEdge
}

"""A location in a connection that can be used for resuming pagination."""
scalar Cursor

"""
A point in time as described by the [ISO
8601](https://en.wikipedia.org/wiki/ISO_8601) standard. May or may not include a timezone.
"""
scalar Datetime

"""All input for the `deleteProductById` mutation."""
input DeleteProductByIdInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String
  id: Int!
}

"""All input for the `deleteProduct` mutation."""
input DeleteProductInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  The globally unique `ID` which will identify a single `Product` to be deleted.
  """
  nodeId: ID!
}

"""The output of our delete `Product` mutation."""
type DeleteProductPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `Product` that was deleted by this mutation."""
  product: Product
  deletedProductId: ID

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """An edge for our `Product`. May be used by Relay 1."""
  productEdge(
    """The method to use when ordering `Product`."""
    orderBy: [ProductsOrderBy!] = PRIMARY_KEY_ASC
  ): ProductsEdge
}

type JwtToken {
  role: String
  exp: Int
  name: String
  userId: Int
}

"""
The root mutation type which contains root level fields which mutate data.
"""
type Mutation {
  """Creates a single `ProductMovement`."""
  createProductMovement(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: CreateProductMovementInput!
  ): CreateProductMovementPayload

  """Creates a single `Product`."""
  createProduct(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: CreateProductInput!
  ): CreateProductPayload

  """Updates a single `Product` using its globally unique id and a patch."""
  updateProduct(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: UpdateProductInput!
  ): UpdateProductPayload

  """Updates a single `Product` using a unique key and a patch."""
  updateProductById(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: UpdateProductByIdInput!
  ): UpdateProductPayload

  """Updates a single `User` using its globally unique id and a patch."""
  updateUser(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: UpdateUserInput!
  ): UpdateUserPayload

  """Updates a single `User` using a unique key and a patch."""
  updateUserById(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: UpdateUserByIdInput!
  ): UpdateUserPayload

  """Deletes a single `Product` using its globally unique id."""
  deleteProduct(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: DeleteProductInput!
  ): DeleteProductPayload

  """Deletes a single `Product` using a unique key."""
  deleteProductById(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: DeleteProductByIdInput!
  ): DeleteProductPayload
  signIn(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: SignInInput!
  ): SignInPayload
  signUp(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: SignUpInput!
  ): SignUpPayload
}

"""An object with a globally unique `ID`."""
interface Node {
  """
  A globally unique identifier. Can be used in various places throughout the system to identify this single value.
  """
  nodeId: ID!
}

"""Information about pagination in a connection."""
type PageInfo {
  """When paginating forwards, are there more items?"""
  hasNextPage: Boolean!

  """When paginating backwards, are there more items?"""
  hasPreviousPage: Boolean!

  """When paginating backwards, the cursor to continue."""
  startCursor: Cursor

  """When paginating forwards, the cursor to continue."""
  endCursor: Cursor
}

type Product implements Node {
  """
  A globally unique identifier. Can be used in various places throughout the system to identify this single value.
  """
  nodeId: ID!
  id: Int!
  name: String!
  desc: String
  updatedAt: Datetime!
  createdAt: Datetime!

  """Reads and enables pagination through a set of `Stock`."""
  stocksByProductId(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `Stock`."""
    orderBy: [StocksOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: StockCondition
  ): StocksConnection!

  """Reads and enables pagination through a set of `ProductMovement`."""
  productMovementsByProductId(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `ProductMovement`."""
    orderBy: [ProductMovementsOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: ProductMovementCondition
  ): ProductMovementsConnection!
  quantity: Int
}

"""
A condition to be used against `Product` object types. All fields are tested for equality and combined with a logical ‘and.’
"""
input ProductCondition {
  """Checks for equality with the object’s `id` field."""
  id: Int

  """Checks for equality with the object’s `name` field."""
  name: String

  """Checks for equality with the object’s `desc` field."""
  desc: String

  """Checks for equality with the object’s `updatedAt` field."""
  updatedAt: Datetime

  """Checks for equality with the object’s `createdAt` field."""
  createdAt: Datetime
}

"""An input for mutations affecting `Product`"""
input ProductInput {
  id: Int
  name: String!
  desc: String
  updatedAt: Datetime
  createdAt: Datetime
}

type ProductMovement implements Node {
  """
  A globally unique identifier. Can be used in various places throughout the system to identify this single value.
  """
  nodeId: ID!
  id: Int!
  productId: Int!
  userId: Int!
  locationCode: String!
  quantity: Int!
  createdAt: Datetime!

  """Reads a single `Product` that is related to this `ProductMovement`."""
  productByProductId: Product

  """Reads a single `User` that is related to this `ProductMovement`."""
  userByUserId: User
}

"""
A condition to be used against `ProductMovement` object types. All fields are
tested for equality and combined with a logical ‘and.’
"""
input ProductMovementCondition {
  """Checks for equality with the object’s `id` field."""
  id: Int

  """Checks for equality with the object’s `productId` field."""
  productId: Int

  """Checks for equality with the object’s `userId` field."""
  userId: Int

  """Checks for equality with the object’s `locationCode` field."""
  locationCode: String

  """Checks for equality with the object’s `quantity` field."""
  quantity: Int

  """Checks for equality with the object’s `createdAt` field."""
  createdAt: Datetime
}

"""An input for mutations affecting `ProductMovement`"""
input ProductMovementInput {
  id: Int
  productId: Int!
  userId: Int!
  locationCode: String!
  quantity: Int!
  createdAt: Datetime
}

"""A connection to a list of `ProductMovement` values."""
type ProductMovementsConnection {
  """A list of `ProductMovement` objects."""
  nodes: [ProductMovement]!

  """
  A list of edges which contains the `ProductMovement` and cursor to aid in pagination.
  """
  edges: [ProductMovementsEdge!]!

  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """
  The count of *all* `ProductMovement` you could get from the connection.
  """
  totalCount: Int
}

"""A `ProductMovement` edge in the connection."""
type ProductMovementsEdge {
  """A cursor for use in pagination."""
  cursor: Cursor

  """The `ProductMovement` at the end of the edge."""
  node: ProductMovement
}

"""Methods to use when ordering `ProductMovement`."""
enum ProductMovementsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  PRODUCT_ID_ASC
  PRODUCT_ID_DESC
  USER_ID_ASC
  USER_ID_DESC
  LOCATION_CODE_ASC
  LOCATION_CODE_DESC
  QUANTITY_ASC
  QUANTITY_DESC
  CREATED_AT_ASC
  CREATED_AT_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

"""
Represents an update to a `Product`. Fields that are set will be updated.
"""
input ProductPatch {
  id: Int
  name: String
  desc: String
  updatedAt: Datetime
  createdAt: Datetime
}

"""A connection to a list of `Product` values."""
type ProductsConnection {
  """A list of `Product` objects."""
  nodes: [Product]!

  """
  A list of edges which contains the `Product` and cursor to aid in pagination.
  """
  edges: [ProductsEdge!]!

  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """The count of *all* `Product` you could get from the connection."""
  totalCount: Int
}

"""A `Product` edge in the connection."""
type ProductsEdge {
  """A cursor for use in pagination."""
  cursor: Cursor

  """The `Product` at the end of the edge."""
  node: Product
}

"""Methods to use when ordering `Product`."""
enum ProductsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  NAME_ASC
  NAME_DESC
  DESC_ASC
  DESC_DESC
  UPDATED_AT_ASC
  UPDATED_AT_DESC
  CREATED_AT_ASC
  CREATED_AT_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

"""The root query type which gives access points into the data universe."""
type Query implements Node {
  """
  Exposes the root query type nested one level down. This is helpful for Relay 1
  which can only query top level fields if they are in a particular form.
  """
  query: Query!

  """
  The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`.
  """
  nodeId: ID!

  """Fetches an object given its globally unique `ID`."""
  node(
    """The globally unique `ID`."""
    nodeId: ID!
  ): Node

  """Reads and enables pagination through a set of `ProductMovement`."""
  allProductMovements(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `ProductMovement`."""
    orderBy: [ProductMovementsOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: ProductMovementCondition
  ): ProductMovementsConnection

  """Reads and enables pagination through a set of `Product`."""
  allProducts(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `Product`."""
    orderBy: [ProductsOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: ProductCondition
  ): ProductsConnection

  """Reads and enables pagination through a set of `Stock`."""
  allStocks(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `Stock`."""
    orderBy: [StocksOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: StockCondition
  ): StocksConnection

  """Reads and enables pagination through a set of `User`."""
  allUsers(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `User`."""
    orderBy: [UsersOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: UserCondition
  ): UsersConnection
  productMovementById(id: Int!): ProductMovement
  productById(id: Int!): Product
  stockById(id: Int!): Stock
  userById(id: Int!): User

  """Reads a single `ProductMovement` using its globally unique `ID`."""
  productMovement(
    """
    The globally unique `ID` to be used in selecting a single `ProductMovement`.
    """
    nodeId: ID!
  ): ProductMovement

  """Reads a single `Product` using its globally unique `ID`."""
  product(
    """The globally unique `ID` to be used in selecting a single `Product`."""
    nodeId: ID!
  ): Product

  """Reads a single `Stock` using its globally unique `ID`."""
  stock(
    """The globally unique `ID` to be used in selecting a single `Stock`."""
    nodeId: ID!
  ): Stock

  """Reads a single `User` using its globally unique `ID`."""
  user(
    """The globally unique `ID` to be used in selecting a single `User`."""
    nodeId: ID!
  ): User
}

"""All input for the `signIn` mutation."""
input SignInInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String
  name: String
  password: String
}

"""The output of our `signIn` mutation."""
type SignInPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String
  jwtToken: JwtToken

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query
}

"""All input for the `signUp` mutation."""
input SignUpInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String
  name: String
  password: String
}

"""The output of our `signUp` mutation."""
type SignUpPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String
  boolean: Boolean

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query
}

type Stock implements Node {
  """
  A globally unique identifier. Can be used in various places throughout the system to identify this single value.
  """
  nodeId: ID!
  id: Int!
  productId: Int!
  locationCode: String!
  quantity: Int!
  updatedAt: Datetime!
  createdAt: Datetime!

  """Reads a single `Product` that is related to this `Stock`."""
  productByProductId: Product
}

"""
A condition to be used against `Stock` object types. All fields are tested for equality and combined with a logical ‘and.’
"""
input StockCondition {
  """Checks for equality with the object’s `id` field."""
  id: Int

  """Checks for equality with the object’s `productId` field."""
  productId: Int

  """Checks for equality with the object’s `locationCode` field."""
  locationCode: String

  """Checks for equality with the object’s `quantity` field."""
  quantity: Int

  """Checks for equality with the object’s `updatedAt` field."""
  updatedAt: Datetime

  """Checks for equality with the object’s `createdAt` field."""
  createdAt: Datetime
}

"""A connection to a list of `Stock` values."""
type StocksConnection {
  """A list of `Stock` objects."""
  nodes: [Stock]!

  """
  A list of edges which contains the `Stock` and cursor to aid in pagination.
  """
  edges: [StocksEdge!]!

  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """The count of *all* `Stock` you could get from the connection."""
  totalCount: Int
}

"""A `Stock` edge in the connection."""
type StocksEdge {
  """A cursor for use in pagination."""
  cursor: Cursor

  """The `Stock` at the end of the edge."""
  node: Stock
}

"""Methods to use when ordering `Stock`."""
enum StocksOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  PRODUCT_ID_ASC
  PRODUCT_ID_DESC
  LOCATION_CODE_ASC
  LOCATION_CODE_DESC
  QUANTITY_ASC
  QUANTITY_DESC
  UPDATED_AT_ASC
  UPDATED_AT_DESC
  CREATED_AT_ASC
  CREATED_AT_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

"""All input for the `updateProductById` mutation."""
input UpdateProductByIdInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  An object where the defined keys will be set on the `Product` being updated.
  """
  productPatch: ProductPatch!
  id: Int!
}

"""All input for the `updateProduct` mutation."""
input UpdateProductInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  The globally unique `ID` which will identify a single `Product` to be updated.
  """
  nodeId: ID!

  """
  An object where the defined keys will be set on the `Product` being updated.
  """
  productPatch: ProductPatch!
}

"""The output of our update `Product` mutation."""
type UpdateProductPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `Product` that was updated by this mutation."""
  product: Product

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """An edge for our `Product`. May be used by Relay 1."""
  productEdge(
    """The method to use when ordering `Product`."""
    orderBy: [ProductsOrderBy!] = PRIMARY_KEY_ASC
  ): ProductsEdge
}

"""All input for the `updateUserById` mutation."""
input UpdateUserByIdInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  An object where the defined keys will be set on the `User` being updated.
  """
  userPatch: UserPatch!
  id: Int!
}

"""All input for the `updateUser` mutation."""
input UpdateUserInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  The globally unique `ID` which will identify a single `User` to be updated.
  """
  nodeId: ID!

  """
  An object where the defined keys will be set on the `User` being updated.
  """
  userPatch: UserPatch!
}

"""The output of our update `User` mutation."""
type UpdateUserPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `User` that was updated by this mutation."""
  user: User

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """An edge for our `User`. May be used by Relay 1."""
  userEdge(
    """The method to use when ordering `User`."""
    orderBy: [UsersOrderBy!] = PRIMARY_KEY_ASC
  ): UsersEdge
}

type User implements Node {
  """
  A globally unique identifier. Can be used in various places throughout the system to identify this single value.
  """
  nodeId: ID!
  id: Int!
  name: String!
  role: String!
  passwordHash: String!
  updatedAt: Datetime!
  createdAt: Datetime!

  """Reads and enables pagination through a set of `ProductMovement`."""
  productMovementsByUserId(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `ProductMovement`."""
    orderBy: [ProductMovementsOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: ProductMovementCondition
  ): ProductMovementsConnection!
}

"""
A condition to be used against `User` object types. All fields are tested for equality and combined with a logical ‘and.’
"""
input UserCondition {
  """Checks for equality with the object’s `id` field."""
  id: Int

  """Checks for equality with the object’s `name` field."""
  name: String

  """Checks for equality with the object’s `role` field."""
  role: String

  """Checks for equality with the object’s `passwordHash` field."""
  passwordHash: String

  """Checks for equality with the object’s `updatedAt` field."""
  updatedAt: Datetime

  """Checks for equality with the object’s `createdAt` field."""
  createdAt: Datetime
}

"""
Represents an update to a `User`. Fields that are set will be updated.
"""
input UserPatch {
  id: Int
  name: String
  role: String
  passwordHash: String
  updatedAt: Datetime
  createdAt: Datetime
}

"""A connection to a list of `User` values."""
type UsersConnection {
  """A list of `User` objects."""
  nodes: [User]!

  """
  A list of edges which contains the `User` and cursor to aid in pagination.
  """
  edges: [UsersEdge!]!

  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """The count of *all* `User` you could get from the connection."""
  totalCount: Int
}

"""A `User` edge in the connection."""
type UsersEdge {
  """A cursor for use in pagination."""
  cursor: Cursor

  """The `User` at the end of the edge."""
  node: User
}

"""Methods to use when ordering `User`."""
enum UsersOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  NAME_ASC
  NAME_DESC
  ROLE_ASC
  ROLE_DESC
  PASSWORD_HASH_ASC
  PASSWORD_HASH_DESC
  UPDATED_AT_ASC
  UPDATED_AT_DESC
  CREATED_AT_ASC
  CREATED_AT_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}
